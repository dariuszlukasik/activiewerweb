<?php
		
		$host="mysql.cba.pl"; // Host name 
		$username="dariusz"; // Mysql username 
		$password="alamakota"; // Mysql password 
		$db_name="activiewer_cba_pl"; // Database name 
		$tbl_name="members"; // Table name 


		include 'EnergyObject.php';
		include 'ReportObject.php';

		Class DataBaseAdapter { 
		
			private $kcalDay;
			private $kcalNow;
			private $time;
			private $dateDisplay;
			private $minDateInTableActivity;
			private $maxDateInTableActivity;
			private $energyObject;
			private $reportObject;	
			public $tim;
			private $link;
			
			
			public function openConnectionDataBase($dataBaseAdres, $user, $password ){
				// Connect to database server
				$this->link = mysqli_connect($dataBaseAdres, $user, $password) or die (mysql_error ());
				if(!$this->link){
					die('Could not connect: ' . mysql_error());
				}
				//echo 'Connected successfully';
				// Select database
				mysqli_select_db($this->link, "activiewer_cba_pl") or die(mysql_error());
			}
		   	
		   	public function getAndSetClassFieldFromDataBase($day) {
				$this->minDateInTableActivity = $this->getTimeFirstExistingDataInDataBase();
				$this->maxDateInTableActivity = $this->getTimeLastExistingDataInDataBase();
				$this->energyObject = new EnergyObject; 
				$this->reportObject = new ReportObject;

				$this->openConnectionDataBase("mysql.cba.pl", "dariusz", "alamakota");
				date_default_timezone_set("Europe/Warsaw");

				if($day == null) {
					$lastExistingTime= $this->maxDateInTableActivity;
					$this->dateDisplay = date("m.d.Y", $lastExistingTime);
					$firstExistingTime = ($lastExistingTime -86400);
					$strSQL = "SELECT * FROM activity  WHERE time < " . $lastExistingTime . " AND time > " . $firstExistingTime .";";
				}
				else {
					$this->dateDisplay = date("m.d.Y", $day);
					$endOfDay = $day + 86400;
					$strSQL = "SELECT * FROM activity WHERE time > " . $day . " AND time < " . $endOfDay ." ;";
				}
				// Execute the query (the recordset $rs contains the result)
				$rs = mysqli_query($this->link, $strSQL);
				// Loop the recordset $rs
				// Each row will be made into an array ($row) using mysql_fetch_array
				while($row = mysqli_fetch_array($rs)) {
					$this->kcalDay[] =$row['kcalDay'] ;
					$this->kcalNow[] = $row['kcalNow'] ;
					$this->time[] =  date("H:i", $row['time'] );
				 }
				if($day == null) {
					$strSQL = "SELECT * from activity_statistic ORDER BY day DESC LIMIT 1 ;";
					$strSQL1 = "SELECT * from activity_report WHERE day =1446465600 ;" ;
				}
				else {
					//day wysyłany z html jest ustawiony na północ, a w bazie rekordy są wkładane
					//codziennie o 13:00;
					$strSQL = "SELECT *from activity_statistic WHERE day = ". ($day +3600*13) . ";";
					$strSQL1 = "SELECT * from activity_report WHERE day =".($day+3600*13).";" ;
				}

				 
				$rs = mysqli_query($this->link, $strSQL);
				  

				 while($row = mysqli_fetch_array($rs)) {

				 	$this->energyObject->setKcalFastRunning($row['fastrunningE']);
				 	$this->energyObject->setKcalRunning($row['runningE']);
				 	$this->energyObject->setKcalWalking($row['walkingE']);
				 	$this->energyObject->setKcalStanding($row['standingE']);
					$this->energyObject->setKcalSitting($row['sittingE']);
				 	$this->energyObject->setTimeFastRunning($row['fastrunningT']);
				 	$this->energyObject->setTimeRunning($row['runningT']);
				 	$this->energyObject->setTimeWalking($row['walkingT']);
				 	$this->energyObject->setTimeStanding($row['standingT']);
					$this->energyObject->setTimeSitting($row['sittingT']);
					$this->energyObject->setTotalTime($row['globalT']);
					$this->energyObject->setTotalEnergy($row['GlobalE']);

				}
				$tim;
				$energy;
				$activity;
				$duration;
				$rs = mysqli_query($this->link, $strSQL1);
				while($row = mysqli_fetch_array($rs)){
					$this->reportObject->addTime($row['time']);
					$this->reportObject->addEnergy($row['energy']);
					$this->reportObject->addActivity($row['activity']);
					$this->reportObject->addDuration($row['duration']);;

				}
				// Close the database connection
				mysqli_close($this->link);				
			}
			
			


			private function getTimeLastExistingDataInDataBase(){
				$getLast = "DESC";
				return $this->getTimeLastOrFirstExistingDataInDataBase($getLast);
			}
			private function getTimeFirstExistingDataInDataBase(){
				$getFirst = "ASC";
				return $this->getTimeLastOrFirstExistingDataInDataBase($getFirst);
			}
			private function getTimeLastOrFirstExistingDataInDataBase($firstOrLast){
				$this->openConnectionDataBase("mysql.cba.pl", "dariusz", "alamakota");		// SQL query
				$strSQL = "SELECT time FROM `activity` ORDER BY time " .$firstOrLast. " LIMIT 1; ";
				$rs = mysqli_query($this->link, $strSQL);
				while($row = mysqli_fetch_array($rs)) {
					$lastTime =$row['time'];
				}
				mysqli_close($this->link);
				return $lastTime;
			}
		
			public function getkcalDay() {
				return $this->kcalDay; 
			}
			public function getkcalNow() {
				return $this->kcalNow; 
			}
			public function getTime() {
				return $this->time;
			}

			public function getMinDateInTableActivity() {
				return $this->minDateInTableActivity;
			}

			public function getMaxDateInTableActivity() {
				return $this->maxDateInTableActivity;
			}

			public function getTimeToDisplay() {
				return $this->dateDisplay;	
			}		
			public function getEnergyObject() {
				return $this->energyObject;	
			}	
			public function getReportObject() {
				return $this->reportObject;
			}
		
		}
		
		
?>