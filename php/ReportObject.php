<?php 
	
	Class ReportObject {
		private $timeForm;
		private $energy;
		private $activity;
		private $duration;
		private $resultToTable;

		public function addTime($time){
			$this->timeForm[] =$time;
			$this->resultToTable[]=$time;
		}

		public function addEnergy($energy){
			$this->energy[] =$energy;
			$this->resultToTable[]=$energy; 
		}
		public function addActivity($activity){
			$this->activity[] =$activity;
			$this->resultToTable[]=$activity; 
		}
		public function addDuration($duration){
			$this->duration[] =$duration; 
			$this->resultToTable[]=$duration;
		}

		public function getTime(){
			return $this->timeForm;
		}
		public function getEnergy(){
			return $this->energy; 
		}
		public function getActivity(){
			return $this->activity;
		}	
		public function getDuration(){
			return $this->duration;
		}
		public function getResultToTable(){
			return $this->resultToTable;
		}
	}

?>