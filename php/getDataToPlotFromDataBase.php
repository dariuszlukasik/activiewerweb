<?php

		include 'DataBaseAdapter.php';

		class Response { 
			public $kcalDay ; 
			public $kcalNow ;
			public $time;
			public $dateDisplay ;
			public $maxTime;
			public $minTime;
			public $energyObject;
			public $reportObject;
		}
		class EnergyObjectInResponse {
			public $kcalFastRunning;
			public $kcalRunning;
			public $kcalWalking;
			public $kcalStanding;
			public $kcalSitting;
			public $totalEnergy;
			public $timeFastRunning;
			public $timeRunning;
			public $timeWalking;
			public $timeStanding;
			public $timeSitting;
			public $totalTime;

			public function __construct($energyObjectOrigin) { 
				$this->kcalFastRunning = $energyObjectOrigin->getKcalFastRunning();
				$this->kcalRunning =$energyObjectOrigin->getKcalRunning();
				$this->kcalWalking =$energyObjectOrigin->getKcalWalking();
				$this->kcalStanding =$energyObjectOrigin->getKcalStanding();
				$this->kcalSitting =$energyObjectOrigin->getKcalSitting();
				$this->totalEnergy =$energyObjectOrigin->getTotalEnergy();
				$this->timeFastRunning =$energyObjectOrigin->getTimeFastRunning();
				$this->timeRunning =$energyObjectOrigin->getTimeRunning();
				$this->timeWalking =$energyObjectOrigin->getTimeWalking();
				$this->timeStanding =$energyObjectOrigin->getTimeStanding();
				$this->timeSitting =$energyObjectOrigin->getTimeSitting();
				$this->totalTime =$energyObjectOrigin->getTotalTime();
				//echo 'Obiekt klasy EnergyObjectInResponse został stworzony.<br/>';  
			}
		} 
		
		class ReportObjectInResponse {
			//public $time;
			//public $energy;
			//public $activity;
			//public $duration;
			public $resultToTable;
			public function __construct($reportObjectOrigin) {
				//$this->time = $reportObjectOrigin->getTime(); 
				//$this->energy =$reportObjectOrigin->getEnergy();
				//$this->activity = $reportObjectOrigin->getActivity(); 
				//$this->duration = $reportObjectOrigin->getDuration();
				$this->resultToTable = $reportObjectOrigin->getResultToTable();
				//echo 'Obiekt klasy ReportObjectInResponse został stworzony.<br/>';
			} 

		}		

		$dataBaseHandle = new DataBaseAdapter;
		$dataBaseHandle->getAndSetClassFieldFromDataBase($_GET['day']);
		$response = new Response;

		$response->kcalDay = $dataBaseHandle->getkcalDay(); 
		$response->kcalNow = $dataBaseHandle->getkcalNow();
		$response->time= $dataBaseHandle->getTime();
		$response->dateDisplay = $dataBaseHandle-> getTimeToDisplay();
		$response->minTime = $dataBaseHandle->getMinDateInTableActivity();
		$response->maxTime = $dataBaseHandle->getMaxDateInTableActivity();
		$energyObjectInResponse = new EnergyObjectInResponse($dataBaseHandle->getEnergyObject());
		$reportObjectInResponse = new ReportObjectInResponse($dataBaseHandle->getReportObject());
		$response->energyObject = $energyObjectInResponse; 
		$response->reportObject = $reportObjectInResponse;
		$jsonRespone = json_encode($response);
		if($_GET['day']!=null){
			echo $jsonRespone;
		}
		
		
?>