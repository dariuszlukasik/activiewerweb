<?php 
	
	Class EnergyObject {
		
		private $kcalFastRunning;
		private $kcalRunning;
		private $kcalWalking;
		private $kcalStanding;
		private $kcalSitting;
		private $totalEnergy;
		private $timeFastRunning;
		private $timeRunning;
		private $timeWalking;
		private $timeStanding;
		private $timeSitting;
		private $totalTime;

		public function setKcalFastRunning($kcalFastRunning) {
			$this->kcalFastRunning = $kcalFastRunning;
		}

		public function setKcalRunning($kcalRunning) {
			$this->kcalRunning = $kcalRunning;
		}
		public function setKcalWalking($kcalWalking) {
			$this->kcalWalking = $kcalWalking;
		}
		public function setKcalStanding($kcalStanding) {
			$this->kcalStanding = $kcalStanding;
		}
		public function setKcalSitting($kcalSitting) {
			$this->kcalSitting = $kcalSitting;
		}
		public function setTimeFastRunning($timeFastRunning) {
			$this->timeFastRunning = $timeFastRunning;
		}
		public function setTimeRunning($timeRunning) {
			$this->timeRunning = $timeRunning;
		}
		public function setTimeWalking($timeWalking) {
			$this->timeWalking = $timeWalking;
		}
		public function setTimeStanding($timeStanding) {
			$this->timeStanding = $timeStanding;
		}
		public function setTimeSitting($timeSitting) {
			$this->timeSitting = $timeSitting;
		}
		public function setTotalEnergy($totalEnergy) {
			$this->totalEnergy = $totalEnergy;
		}
		public function setTotalTime($totalTime) {
			$this->totalTime = $totalTime;
		}
		public function getKcalFastRunning() {
			return $this->kcalFastRunning ;
		}

		public function getKcalRunning() {
			return $this->kcalRunning;
		}
		public function getKcalWalking() {
			return $this->kcalWalking;
		}
		public function getKcalStanding() {
			return $this->kcalStanding ;
		}
		public function getKcalSitting() {
			return $this->kcalSitting ;
		}
		public function getTimeFastRunning() {
			return $this->timeFastRunning ;
		}
		public function getTimeRunning() {
			return $this->timeRunning ;
		}
		public function getTimeWalking() {
			return $this->timeWalking ;
		}
		public function getTimeStanding() {
			return $this->timeStanding ;
		}
		public function getTimeSitting() {
			return $this->timeSitting ;
		}
		public function getTotalEnergy() {
			return $this->totalEnergy ;
		}
		public function getTotalTime() {
			return $this->totalTime;
		}




	}




?>