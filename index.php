<?php
if(isset($_SESSION['login_user'])){
	header("location: profile.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="Stylesheet" type="text/css" href="css/arkusz.css" />	
<link rel="Stylesheet" type="text/css" href="css/fontello.css" />
	
</head>

<body>
	<b>
	<div id="naglowek" >actiViewer </div>
	<div class="menu">
		<ol>
			<li><a href="../index.php">STRONA GŁÓWNA</a></li>
			<li><a href="#">O NAS</a>
				<ul>
					<li><a href="#">Kim jesteśmy</a></li>
					<li><a href="#">Hobby</a></li>

				</ul>
			</li>
			<li><a href="#">PROJEKTY </a>
				<ul>
					<li><a href="#">actiViewer </a></li>
					<li><a href="#">Wyszukiwarka Osób</a></li>
					<li><a href="#">Klocki</a></li>
					<li><a href="#">Silnik Dc</a></li>
				</ul>
			</li>
			<li><a href="login.html">LOG IN</a></li>
			<li><a href="#">KONTAKT</a></li>
		</ol>
	</b>
	</div>
	<div id="ContentInMainPage">
		<div id= "ContentInMainPageText">
			<br><br><br>
			<p class><b>actiViewer</b> jest aplikacją mobilną do śledzenie aktywności fizycznej. Rozpoznaje aktywności wykonywane przez 
			człowieka, w związku z tym użytkownik nie musi sam określać jaką czynność wykonuje, tak jak jest to wielu konkurencyjnych
			produktach. Aplikacja resjestruje aktywności oraz szacuje z dużą dokładnością ilość zużytych kalorii. W chwili obecnej 
			napisana jest na system Android, jednak w przyszłości planuje się rozszerzyć zasięg na inne platformy. 
			Serwis internetowy powstaje w celu integracji danych zebranych przez smartfon. Aplikacja wkrótce będzie można pobrać 
			pod adresem : <a href="actiViewer.download.pl">tutaj</a>. Aby uzyskać dostęp do systemu monitorowania aktywnościami należy zalogować 
			się klikając w zakładkę LogIn. System można zobaczyć logując się na konto tymczasowe : 
			login : <b>test</b>  
			hasło : <b>test</b>  
			</p>  
		</div>
		<div id= "ContentInMainPageImage">
			<a href="login.html"></a>
		</div>
		<div id="ContentInMainPageTextRight"></div>
	</div>
	<div  class="socials" >
		<div class="socialdivs">
			<div class="fb"><i class="icon-facebook"></i></div>
			<div class="yt"><i class="icon-youtube"></i></div>
			<div class="tw"><i class="icon-twitter"></i></div>
			<div class="gplus"><i class="icon-gplus"></i></div>
			<div style="clear:both"></div>
		</div>
	</div>

	
	<div class= "footer">actiViewer  &copy;2015</div>
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/przyklej.js"></script>
	
</body>
</html>
	
